% @doc
% this is like the unix tree command
-module(ohft_tree).

-export([
    tree/1
]).
-export_type([
    tree/0
]).


-record(w,
        {filesystem_verbose_path :: string(),
         chidrin                 :: [tree()]}).
-type wood() :: #w{}.

-record(g,
        {filesystem_verbose_path :: string()}).
-type green() :: #g{}.

-type tree() :: {stem, wood()}
              | {leaf, green()}.



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% API
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-spec tree(Directory :: file:name_all()) -> tree().
% @doc
% Like the unix tree command

tree(Directory) ->
    FullName = filename:absname(Directory),
    Wood = #w{filesystem_verbose_path = FullName,
              chidrin                 = []},
    Tree = {stem, Wood},
    Tree.
